package com.vaclavhnizda.sudoku;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;

// Preference Page for the game
// - selected from Main screen's menu drop-down.

public class Preferences extends PreferenceActivity 
				implements OnSharedPreferenceChangeListener {
	
	private static final String hints_Pref = "hints";
	private static final boolean hints_Default = true;

	@Override
	protected void onResume() {
	    super.onResume();
	    getPreferenceScreen().getSharedPreferences()
	            .registerOnSharedPreferenceChangeListener(this);
	}

	@Override
	protected void onPause() {
	    super.onPause();
	    getPreferenceScreen().getSharedPreferences()
	            .unregisterOnSharedPreferenceChangeListener(this);
	}	
	
	@Override
	protected void onCreate(Bundle saveInstanceState){
		super.onCreate(saveInstanceState);
		addPreferencesFromResource(R.xml.settings);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences sharedPrefs, String key) {
		//Log.d("Sudoku", "Setting changed");
		
	}
	
	public static  boolean getHints(Context context) {
		SharedPreferences cache =  PreferenceManager.getDefaultSharedPreferences(context);
		return cache.getBoolean(hints_Pref, hints_Default);
	}
}
