package com.vaclavhnizda.sudoku;

import android.app.Activity;
import android.os.Bundle;

// Info Page for the Game
// - selected from Main screen's menu drop-down.

public class About extends Activity{
	
	//Displays game information from res/layout/about.xml
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.about);
	}
}
