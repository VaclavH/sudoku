package com.vaclavhnizda.sudoku;

import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog.Builder;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;
import android.app.AlertDialog;
import android.content.DialogInterface;

// Main Page with game options for players
// - Reached automatically at the start of the game.

public class Sudoku extends Activity implements OnClickListener {

	private static final String TAG = "Sudoku";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		
		//Set up click listeners for all the buttons
		View continueButton = findViewById(R.id.continue_button);
		continueButton.setOnClickListener(this);
		View newButton = findViewById(R.id.new_button);
		newButton.setOnClickListener(this);
		View aboutButton = findViewById(R.id.about_button);
		aboutButton.setOnClickListener(this);
		View exitButton = findViewById(R.id.exit_button);
		exitButton.setOnClickListener(this);
		View helperButton = findViewById(R.id.helper_button);
		helperButton.setOnClickListener(this);
		
		}

	
	@Override
	public void onClick(View v){
		switch (v.getId()) 
		{
		case R.id.continue_button:
			startGame(Game.DIFFICULTY_CONTINUE);
			break;
		case R.id.new_button:
			openNewGameDialog();
			break;
		case R.id.helper_button:
			//Log.d("Sudoku", "Helper Button Pressed");
			startGame(Game.DIFFICULTY_BLANK);
			break;
		case R.id.about_button:
			Intent i = new Intent(this, About.class);
			startActivity(i);
			break;
		case R.id.exit_button:
			finish();
			break;
		}
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		super.onCreateOptionsMenu(menu);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menu,  menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item){
		//Log.d(TAG, "menu selected!");
//issue here
		switch (item.getItemId()){
		case R.id.settings:
			startActivity(new Intent(this, Preferences.class));
			return true;
		}
		return false;
	}
	
	
	private void openNewGameDialog() {
		// Request player to choose difficulty
		Builder difficultyNotice = new AlertDialog.Builder(this);
		difficultyNotice.setTitle(R.string.new_game_title);
		difficultyNotice.setItems(R.array.difficulty, 
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialoginterface, int i)
					{
						startGame(i);
					}
				});
		difficultyNotice.show();
	}
	
	//Game created and difficulty entered
	private void startGame(int i) {
		//Log.d(TAG, "clicked on " + i);
		Intent intent = new Intent(Sudoku.this, Game.class);
		intent.putExtra(Game.KEY_DIFFICULTY, i);	//Data chached for Game (class) KEY_DIFFICULTY (variable)
		startActivity(intent);
	}
	

}
