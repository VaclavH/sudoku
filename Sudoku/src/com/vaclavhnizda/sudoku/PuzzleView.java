package com.vaclavhnizda.sudoku;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Paint.FontMetrics;
import android.graphics.Paint.Style;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AnimationUtils;

public class PuzzleView extends View{
	
	private static final String TAG = "Sudoku";
	private final Game game; //Instance of the game class (logic)
	private float tileWidth;	//tileWidth of one tile
	private float tileHeight;	//tileHeight of one tile
	private int selX;		//X index of selection
	private int selY;		//Y index of selection
	private final Rect selRect = new Rect();
	private int boardHeight;	//game board tileHeight
	private int boardWidth;		//game board tileWidth
	//Board placement parameters
	private int displaceH = 1;	//displacement to make game centered
	private int displaceW = 1;	//displacement to make game centered


	
	public PuzzleView(Context context) {
		super(context);
		this.game = (Game) context;
		setFocusable(true);
		setFocusableInTouchMode(true);
		
		
	}
	
	// Sets dimensions of the board. 
	// If statement finds shorter side and creates a square board. 
	// On screen rotation, aka resize, new dimensions are set for the tileWidth/tileHeight of the game as well.
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		if(w > h) {
			h=h-3;//offset for slight visual improvement
			tileWidth = h / 9f;
			tileHeight = h / 9f;
		}
		else {
			w=w-3;//offset for slight visual improvement
			tileWidth = w / 9f;
			tileHeight = w / 9f;
		}
//		Old settings:
//		tileWidth = w / 9f;
//		tileHeight = h / 9f;
		// Sets up Rectangle for highlighting
//		getRect(selX, selY, selRect);
		//Log.d(TAG, "onSizedChanged: tileWidth " + tileWidth + ", tileHeight " + tileHeight);
		super.onSizeChanged(w, h, oldw, oldh);
	}
	
	
	private void getRect(int x, int y, Rect rect) {
		//	 set square: 	Left		Top					Right						Bottom
		rect.set((int) (x * tileWidth)+displaceW, (int) (y * tileHeight)+displaceH, (int) (x * tileWidth + tileWidth)+displaceW, (int) (y * tileHeight + tileHeight)+displaceH);
	}
	
	@SuppressLint("DrawAllocation")
	@Override
	protected void onDraw(Canvas canvas) {
		//Square board size parameters
		boardHeight = getHeight();
		boardWidth = getWidth();
		//Dimensions are configured
		if (boardWidth > boardHeight){
			displaceW = (boardWidth-boardHeight)/2;
			boardWidth = boardHeight;
		}
		else{
			displaceH = (boardHeight-boardWidth)/2;
			boardHeight = boardWidth;
		}
		
		
		// Draw the background
		Paint background = new Paint();
		background.setColor(getResources().getColor(R.color.puzzle_background));
		canvas.drawRect(0, 0, getWidth(), getHeight(), background);
		
		
		// 1.Draw the hints
		// 1-A Pick a hint color based on #moves left
		if(Preferences.getHints(game))
		{
			Paint hint = new Paint();
			int c[] = { getResources().getColor(R.color.puzzle_hint_0),
					getResources().getColor(R.color.puzzle_hint_1),
					getResources().getColor(R.color.puzzle_hint_2), };
			Rect r = new Rect();
			for (int i = 0; i < 9; i++) {
				for (int j = 0; j < 9 ; j++) {
					if(this.game.getOriginalTileString(i, j) == 0)
					{
						int movesleft = 9 - game.getUsedTiles(i, j).length;
						if (movesleft < c.length) {
							getRect(i, j, r);
							hint.setColor(c[movesleft]);
							canvas.drawRect(r, hint);
						}
					}
				}
			}	
		}
		
		// 2.Draw the selection
		//Log.d(TAG, "selRect=" + selRect);
		Paint selected = new Paint();
		selected.setColor(getResources().getColor(R.color.puzzle_selected));
		canvas.drawRect(selRect,  selected);
		
		
		// 3.Draw the board (this covers up the edges of the hints and selection blocks nicely)
		// 3-A Define colors for the grid lines
		Paint dark = new Paint();
		dark.setColor(getResources().getColor(R.color.puzzle_dark));

		Paint hilite = new Paint();
		hilite.setColor(getResources().getColor(R.color.puzzle_hilite));

		Paint light = new Paint();
		light.setColor(getResources().getColor(R.color.puzzle_light));
		
		// 3-B Draw the minor grid lines
		for (int i = 0 ; i < 10 ; i++){
			canvas.drawLine(0 + displaceW, i * tileHeight + displaceH, 		boardWidth + displaceW, i * tileHeight + displaceH, 	light);
			canvas.drawLine(0 + displaceW, i * tileHeight + displaceH + 1, 	boardWidth + displaceW, i * tileHeight + displaceH + 1, hilite);
			canvas.drawLine(i * tileWidth + displaceW, 		0 + displaceH, i * tileWidth + displaceW,     boardHeight + displaceH, 	light);
			canvas.drawLine(i * tileWidth + displaceW + 1, 	0 + displaceH, i * tileWidth + displaceW + 1, boardHeight + displaceH, 	hilite);
		}
		
		// 3-C Draw Major Grid Lines. Go to 12 to add line to end of screen to complete border.
		for (int i = 0 ; i < 10 ; i++){
			if (i % 3 != 0 )
				continue;
			canvas.drawLine(0 + displaceW,  i * tileHeight + displaceH,     boardWidth + displaceW,  i * tileHeight + displaceH, dark);
			canvas.drawLine(0 + displaceW,  i * tileHeight + displaceH + 1, boardWidth + displaceW,  i * tileHeight + displaceH + 1,  dark);
			canvas.drawLine(0 + displaceW,  i * tileHeight + displaceH - 1, boardWidth + displaceW,  i * tileHeight + displaceH - 1,  dark);
			canvas.drawLine(0 + displaceW,  i * tileHeight + displaceH + 2, boardWidth + displaceW,  i * tileHeight + displaceH + 2,  dark);
			canvas.drawLine(0 + displaceW,  i * tileHeight + displaceH - 2, boardWidth + displaceW,  i * tileHeight + displaceH - 2,  dark);
			canvas.drawLine(i * tileWidth + displaceW, 	   0 + displaceH, i * tileWidth + displaceW,  	  boardHeight + displaceH,  dark);
			canvas.drawLine(i * tileWidth + displaceW + 1, 0 + displaceH, i * tileWidth + displaceW + 1,  boardHeight + displaceH,  dark);
			canvas.drawLine(i * tileWidth + displaceW - 1, 0 + displaceH, i * tileWidth + displaceW - 1,  boardHeight + displaceH,  dark);
			canvas.drawLine(i * tileWidth + displaceW + 2, 0 + displaceH, i * tileWidth + displaceW + 2,  boardHeight + displaceH,  dark);
			canvas.drawLine(i * tileWidth + displaceW - 2, 0 + displaceH, i * tileWidth + displaceW - 2,  boardHeight + displaceH,  dark);
		}
				
		
		// 4.Draw the numbers - (putting the on top keeps them the same color / no blend)
		// 4-A Define color and style for numbers
		Paint foreground = new Paint(Paint.ANTI_ALIAS_FLAG);
		foreground.setColor(getResources().getColor(R.color.puzzle_foreground));
		foreground.setStyle(Style.FILL);
		foreground.setTextSize(tileHeight * 0.75f);
		foreground.setTextScaleX(tileWidth/tileHeight);
		foreground.setTextAlign(Paint.Align.CENTER);
		
		// 4-A2 Set up different icon color for orginal numbers
		Paint originalForeground = new Paint(foreground);
		originalForeground.setColor(getResources().getColor(R.color.puzzle_original_forground));
		
		// 4-B Draw the number in the center of the tile
		FontMetrics fm = foreground.getFontMetrics();
		
		// 4-C Centering in X: use alignment (and X at midpoint)
		float x = tileWidth / 2;
		
		// 4-D Centering in Y: measure ascent/descent first
		float y = tileHeight / 2 - (fm.ascent + fm.descent) / 2; 
		
		// 4-E Drawing text to screen
		for (int i = 0 ; i < 9 ; i++) {
			for (int j = 0 ; j < 9 ; j++) {
				//figures out which pieces are original and which are user set. Differentiates in color scheme.
				if(this.game.getOriginalTileString(i, j) == 0){
					canvas.drawText(this.game.getTileString(i,j), i * tileWidth + x + displaceW, j * tileHeight + y + displaceH, foreground);
				}else{
					canvas.drawText(this.game.getTileString(i,j), i * tileWidth + x + displaceW, j * tileHeight + y + displaceH, originalForeground);					
				}
			}
		}
			

	}
	
	//Key Commands from user
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		//Log.d(TAG, "onKeyDown: keycode=" + keyCode + ", event=" + event);
		
		switch (keyCode) {
			
			// Keys for changing the number in the selection
			case KeyEvent.KEYCODE_0:
			case KeyEvent.KEYCODE_SPACE: setSelectedTile(0); break;
			case KeyEvent.KEYCODE_1: setSelectedTile(1); break;
			case KeyEvent.KEYCODE_2: setSelectedTile(2); break;
			case KeyEvent.KEYCODE_3: setSelectedTile(3); break;
			case KeyEvent.KEYCODE_4: setSelectedTile(4); break;
			case KeyEvent.KEYCODE_5: setSelectedTile(5); break;
			case KeyEvent.KEYCODE_6: setSelectedTile(6); break;
			case KeyEvent.KEYCODE_7: setSelectedTile(7); break;
			case KeyEvent.KEYCODE_8: setSelectedTile(8); break;
			case KeyEvent.KEYCODE_9: setSelectedTile(9); break;
			case KeyEvent.KEYCODE_ENTER:
			case KeyEvent.KEYCODE_DPAD_CENTER:
				game.showKeypadOrError(selX, selY);
				break;
			
			// Keys for moving the selection around
			case KeyEvent.KEYCODE_DPAD_UP:
				select(selX, selY - 1);
				break;
			case KeyEvent.KEYCODE_DPAD_DOWN:
				select(selX, selY + 1);
				break;
			case KeyEvent.KEYCODE_DPAD_LEFT:
				select(selX - 1, selY);
				break;
			case KeyEvent.KEYCODE_DPAD_RIGHT:
				select(selX + 1, selY);
				break;
			default:
				return super.onKeyDown(keyCode,  event);
		}
		return true;
	}
	
	// Figure out location user touched the screen
	@Override
	public boolean onTouchEvent(MotionEvent event) {
		if (event.getAction() != MotionEvent.ACTION_DOWN)
			return super.onTouchEvent(event);
		
		// Confirm first the user touched inside the board game
		if(event.getX() >= displaceW && event.getX() <= (boardWidth+displaceW) && event.getY() >= displaceH && event.getY() <= (boardHeight+displaceH))
		{
			// Figure out which cube the finger was closest to on the touch
			select((int) ((event.getX()-displaceW) / tileWidth), (int) ((event.getY()-displaceH) / tileHeight));
		
			//Display Number options for that location
			game.showKeypadOrError(selX, selY);
			//Log.d(TAG, "onTouchEvent: x " + selX + ", y " + selY);
		}
		
		
		return true;
	}
	
	// Move selection to new location x/y
	private void select(int x, int y) {
		
		invalidate(selRect);
		selX = Math.min(Math.max(x, 0),  8);
		selY = Math.min(Math.max(y,  0),  8);
		getRect(selX, selY, selRect);
		invalidate(selRect);
		//Log.d("Sudoku","tile selection made at x = " + selX + " and y = " + selY);
		
	}
	
	
	public void setSelectedTile(int tile) {
		if (game.setTileIfValid(selX, selY, tile)) {
			invalidate(); // Whole screen will update, including hints.
			
			//Log.d("Sudoku", "change made in game");
			
			//Board is checked if everything is filled and if so, Completion program initiated
			if(game.boardComplete()){
				
			}else
			{
				//Log.d("Sudoku", "Game not finnished");
			}
			
		} else {
			// Number is not valid for this tile
			//Log.d(TAG, "setSelectedTile: invalid: " + tile);
			startAnimation(AnimationUtils.loadAnimation(game, R.anim.shake));
		}
	}
	
}
