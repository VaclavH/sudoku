package com.vaclavhnizda.sudoku;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

public class Winner extends Activity implements OnClickListener{
	
	@Override
	protected void onCreate(Bundle saveInstanceState){
		super.onCreate(saveInstanceState);
		setContentView(R.layout.winner);
		
		//Set up click listener
		View continueButton = findViewById(R.id.winner_continue_button);
		continueButton.setOnClickListener(this);
	}
	
	@Override
	public void onClick(View v){
		switch (v.getId()){
		case R.id.winner_continue_button:
			this.finish();
			break;
		}
	}
	
}
