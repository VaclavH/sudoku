package com.vaclavhnizda.sudoku;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

public class Game extends Activity{
	private static final String TAG = "Sudoku";
	private int used[][][] = new int[9][9][];
	
	protected int[] getUsedTiles(int x, int y) {
		return used[x][y];
	}
	
	//Game difficulties tagged with number values
	public static final String KEY_DIFFICULTY = "com.vaclavhnizda.sudoku.difficulty"; //set by Sudoku class
	protected static final int DIFFICULTY_CONTINUE 	=-1;
	public static final int 	DIFFICULTY_EASY 	= 0;
	public static final int 	DIFFICULTY_MEDIUM 	= 1;
	public static final int 	DIFFICULTY_HARD 	= 2;
	public static final int 	DIFFICULTY_BLANK 	= 3;
	
	private int puzzle[] 				= new int[9 * 9]; 	//This holds all the game pieces
	private int puzzleOriginalNumbers[] = new int[9 * 9];	//this tells you where the original numbers are located: 1 = original, 0 = user entry
	
	private PuzzleView puzzleView;
	private static final String PREF_PUZZLE = "puzzle";
	private static final String PREF_PUZZLE_ORIGIN = "puzzleorigin";

	
	//Initial 3 Strings of different levels of Sudoku. String can be converted to array of numbers for the board. 
	   private final String easyPuzzle =
	      "360000000004230800000004200" +
	      "170460003820000014500013020" +
	      "001900000007048300000000045";
	   private final String mediumPuzzle =
	      "650000070000506000014000065" +
	      "007009000002314700000700800" +
	      "500000630000201000030000097";
	   private final String hardPuzzle =
	      "009000000080605020501078000" +
	      "000000700706040102004007000" +
	      "000720903090301080000000600";
	   private final String blankPuzzle =
		  "000000000000000000000000000" +
		  "000000000000000000000000000" +
		  "000000000000000000000000000";

	
	   
	   
	//On Create configures the game and then displays the Puzzle View
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Log.d(TAG, "onCreate");
		//Retrieve difficulty
		int diff = getIntent().getIntExtra(KEY_DIFFICULTY, DIFFICULTY_EASY);
		puzzle = getPuzzle(diff);			
		calculateUsedTiles();
		
		// Create an instance of PuzzleView and configure it
		puzzleView = new PuzzleView(this);
		setContentView(puzzleView);
		// Send user to PuzzleView
		puzzleView.requestFocus();
		
	}
	
	
	//Save Puzzle when paused
	@Override
	protected void onPause(){
		super.onPause();
		//Log.d(TAG, "onPause");
		
		//Create "Editor" data file
		Editor saveData = getPreferences(MODE_PRIVATE).edit();
		
		//Create string of puzzle pieces
		String gameString = toPuzzleString(puzzle);
		//Create string of original puzzle piece locations
		String gameOriginString = toPuzzleString(puzzleOriginalNumbers);
		
		//Add puzzle string to data file
		saveData.putString(PREF_PUZZLE,  gameString);
		//Add puzzle originals string to data file
		saveData.putString(PREF_PUZZLE_ORIGIN, gameOriginString);
		//Commit file
		saveData.commit();
		getIntent().putExtra(KEY_DIFFICULTY, DIFFICULTY_CONTINUE);//
	}
	   
	   
	   // Given the difficulty Number:
	   		//1. Load the respective game string of numbers
	   		//2. Pass string to fromPuzzleString() method
	   		//3. Return integer array derived from the string
	   		// TODO: Continue last game
	   private int[] getPuzzle(int difficultyNumber) {
	      String puzz,puzzOrigin;
		  
	      switch (difficultyNumber) {
	      case DIFFICULTY_HARD:
	         puzz = hardPuzzle;
	         this.puzzleOriginalNumbers = fromPuzzleString(hardPuzzle);
	         break;
	      case DIFFICULTY_MEDIUM:
	         puzz = mediumPuzzle;
	         this.puzzleOriginalNumbers = fromPuzzleString(mediumPuzzle);
	         break;
	      case DIFFICULTY_EASY:
	      default:
	         puzz = easyPuzzle;
	         this.puzzleOriginalNumbers = fromPuzzleString(easyPuzzle);
	         break;
	      case DIFFICULTY_CONTINUE:
	    	  puzz = getPreferences(MODE_PRIVATE).getString(PREF_PUZZLE, easyPuzzle);
	    	  puzzOrigin = getPreferences(MODE_PRIVATE).getString(PREF_PUZZLE_ORIGIN, easyPuzzle);
	    	  this.puzzleOriginalNumbers = fromPuzzleString(puzzOrigin);
	    	  break;
	      case DIFFICULTY_BLANK:
	    	  puzz = blankPuzzle;
	    	  this.puzzleOriginalNumbers = fromPuzzleString(blankPuzzle);
	    	  break;
	      }
	      //Set up the Sudoku array list
	      return fromPuzzleString(puzz);
	   }
	
	
	// Keypad dialog created to give number options for the game. 
	// A Toast message is displayed if no moves for this spot remain.
	protected void showKeypadOrError(int x, int y) {
		
		if (puzzleOriginalNumbers[(y * 9) + x] == 0){	//This is set up so that the original numbers can't be changed
			
			int tiles[] = getUsedTiles(x, y);
			if (tiles.length == 9) {
				Toast toast = Toast.makeText(this,  R.string.no_moves_label,  Toast.LENGTH_SHORT);
				toast.setGravity(Gravity.CENTER, 0, 0);
				toast.show();
			}else{
				//Log.d(TAG, "showKeypad: used=" + toPuzzleString(tiles));
				Dialog v = new KeypadDialog(this, tiles, puzzleView);
				v.show();
			}
		}
	}
	
	// Checks if number entered is Zero
	// then confirms the number is not already in use elsewhere
	// finally it sets the square to that number (if valid) and refreshes the screen
	protected boolean setTileIfValid(int x, int y, int value) {
		int tiles[] = getUsedTiles(x, y);
		if (value != 0) {
			for (int tile : tiles) {
				if(tile == value)	//checks the number selected is not on the list of invalid numbers
					return false;
			}
		}
		setTile(x, y, value);
		calculateUsedTiles();
		return true;
	}
	
	
	//Checks each tile and figures out what numbers are currently not possible (already in used by neighbor)
	private void calculateUsedTiles() {
		for (int x = 0 ; x < 9 ; x++) {
			for (int y = 0; y < 9 ; y++) {
					used[x][y] = calculateUsedTiles(x,y);
					//Log.d(TAG, "used[" + x + "][" + y +"] = " + toPuzzleString(used[x][y]));
			}
		}
	}

   //Figure out what tiles have been used (hints) to help limit user's input options
   private int[] calculateUsedTiles(int x, int y) {
	  int c[] = new int[9];
	  // horizontal
	  for (int i = 0; i < 9; i++) { 
	     if (i == x)
	        continue;
	     int t = getTile(i, y);
	     if (t != 0)
	        c[t - 1] = t;
	  }
	  // vertical
	  for (int i = 0; i < 9; i++) { 
	     if (i == y)
	        continue;
	     int t = getTile(x, i);
	     if (t != 0)
	        c[t - 1] = t;
	  }
	  // same cell block
	  int startx = (x / 3) * 3; 
	  int starty = (y / 3) * 3;
	  for (int i = startx; i < startx + 3; i++) {
	     for (int j = starty; j < starty + 3; j++) {
	        if (i == x && j == y)
	           continue;
	        int t = getTile(i, j);
	        if (t != 0)
	           c[t - 1] = t;
	     }
	  }
	  // compress
	  int nused = 0; 
	  for (int t : c) {
	     if (t != 0)
	        nused++;
	  }
	  int c1[] = new int[nused];
	  nused = 0;
	  for (int t : c) {
	     if (t != 0)
	        c1[nused++] = t;
	  }
	  //Log.d(TAG, "Number of available numbers for cell block [x] - " + x + " & [y] - " + y + " is " + c1.length);
	  
	  //returns the tiles that are unavailable for a given block
	  return c1;
	  
	  //an empty array will make a block seem as if any number could be used there
//	      c1 = new int[] {};
//	      return c1;
   }
	   
	   
	   //Converts the game grid numbers into a String
	   static private String toPuzzleString(int[] puz) {
	      StringBuilder buf = new StringBuilder();
	      for (int element : puz) {
	         buf.append(element);
	      }
	      return buf.toString();
	   }
	  
	   //Converts string into an array for the game grid
	   static protected int[] fromPuzzleString(String string) {
	      int[] puz = new int[string.length()];
	      for (int i = 0; i < puz.length; i++) {
	         puz[i] = string.charAt(i) - '0';
	      }
	      return puz;
	   }
	   
	   //Function figures out what number is in use at the requested block
	   private int getTile(int x, int y) {
		   return puzzle[y*9 + x];
	   }
	   //Function figures out if there was an original number at the requested block
	   private int getOriginalTile(int x, int y) {
		   return puzzleOriginalNumbers[y*9 + x];
		   
	   }
	   
	   //Function sets the grid box to a given value
	   private void setTile(int x, int y, int value) {
		   puzzle[y * 9 + x] = value;
	   }
	   
	   //Function returns a string with the value from a given block
	   protected String getTileString(int x, int y) {
		   int v = getTile(x, y);
		   if (v == 0)
			   return"";
		   else return String.valueOf(v);
	   }
	   
	   //Function returns a string with the value originally in this block
	   protected int getOriginalTileString(int x, int y){
		   int v = getOriginalTile(x, y);
		   return v;
	   }

	//Checks squares and sees if game has been completed
	public boolean boardComplete() {
		for (int x = 0 ; x < 9 ; x++) {
			for (int y = 0; y < 9 ; y++) {
				int blockNum = getTile(x,y);
				if (blockNum == 0){
//					Log.d(TAG, "At x - " + x + " y - " + y + " there is no number ");
					return false;
				}//else{Log.d(TAG, "At x - " + x + " y - " + y + " the number is " + blockNum);}
			}
		}
		Log.d("Sudoku","Game Completed!");
		
		// 1. clear saved game in game storage
		
		// 2. load game complete activity
		Intent goal = new Intent(this, Winner.class);
		startActivity(goal);
		// 3. close game activity
		this.finish();
		return true;
	}
	
	public void menu(){
		
	}
}
